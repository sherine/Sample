﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SampleAppNew.Startup))]
namespace SampleAppNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
